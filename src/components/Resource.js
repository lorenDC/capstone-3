import { Row, Col, Accordion, Card } from 'react-bootstrap'

export default function Resource() {
	return (
		<Row className="mt-3 mb-3">
			<Col>
			    <Accordion>
			      <Accordion.Item eventKey="0">
			        <Accordion.Header>CSS Course</Accordion.Header>
			        <Accordion.Body>
			        	<Card.Text>
			        		CSS is the language we use to style an HTML document. CSS describes how HTML elements should be displayed
			         	</Card.Text>  
			        <a className="btn btn-primary" href="https://www.w3schools.com/css/" target="_blank">Read more</a>
			        </Accordion.Body>	        
			      </Accordion.Item>
			    
			      <Accordion.Item eventKey="1">
			        <Accordion.Header>Bootstrap Course</Accordion.Header>
			        <Accordion.Body>
			        	<Card.Text>
				          Bootstrap is the most popular HTML, CSS, and JavaScript framework for developing responsive, mobile-first websites.
			         	</Card.Text>  
			        <a className="btn btn-primary" href="https://www.w3schools.com/bootstrap/" target="_blank">Read more</a>
			        </Accordion.Body>
			      </Accordion.Item>

			      <Accordion.Item eventKey="2">
			        <Accordion.Header>JavaScript Course</Accordion.Header>
			        <Accordion.Body>
			        	<Card.Text>
				          JavaScript is the world's most popular programming language. JavaScript is the programming language of the Web. JavaScript is easy to learn.
			         	</Card.Text>  
			        <a className="btn btn-primary" href="https://www.w3schools.com/js/" target="_blank">Read more</a>
			        </Accordion.Body>
			      </Accordion.Item>
			    </Accordion>
			</Col>
		</Row>
	)
}